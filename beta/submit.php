<?php
$data = json_decode(file_get_contents("php://input"), true);

if(isset($data["email"])) {
    $sender = 'website';
    $recipient = 'hello@nabad.art';
    //$recipient = 'art.nabad@gmail.com';

    $subject = "Call for Artists";
    $message = "First Name: ".$data["firstName"]."\n";
    $message .= "Last Name: ".$data["lastName"]."\n";
    $message .= "Art Category: ".$data["artCategory"]."\n";
    $message .= "Short Bio: ".$data["shortBiography"]."\n";
    $message .= "Statement: ".$data["statement"]."\n";
    $message .= "Email: ".$data["email"]."\n";
    $message .= "Mobile: ".$data["mobile"]."\n";
    $message .= "Website: ".$data["website"]."\n";
    $message .= "Digital Portfolio: ".$data["digitalPortfolio"]."\n";
    $message .= "Instagram: ".$data["instagram"]."\n";
    $message .= "Facebook: ".$data["facebook"]."\n";
    $message .= "Youtube: ".$data["youtube"];


    $headers = array(
        'From' => $sender,
        'Reply-To' => $sender,
        'X-Mailer' => 'PHP/' . phpversion()
    );


    if (mail($recipient, $subject, $message, $headers))
    {
        echo json_encode(array("sent"=>true,"msg"=>"Your message has been sent!"));
    }
    else
    {
        echo json_encode(array("sent"=>false,"msg"=>"Error sending message. Please try again later!"));
    }
}
