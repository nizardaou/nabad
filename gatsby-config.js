module.exports = {
  plugins: [
    {
      resolve: "gatsby-plugin-react-helmet",
    },
    {
      resolve: `gatsby-plugin-routes`,
      options: {
        // this is the path to your routes configuration file
        path: `${__dirname}/src/routes.js`,
      },
    },
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds: ["G-68VJK440EZ"],
      },
    },
    `gatsby-plugin-smoothscroll`,
  ],
};
