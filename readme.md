# Nabad Landing Page

## Running the Gatsby Project

1.  Start by installing Gatsby CLI tool that lets you quickly create new Gatsby-powered sites and run them.

    `npm install -g gatsby-cli` to be installed globaly

    or

    `npm install gatsby-cli` as an npm package

    then add `"develop": "gatsby develop"` to under scripts in package.json

1.  Navigate into the site’s directory and start it up.

    cd nabad-landing-page/

    `gatsby develop`

    or

    `npm run develop`

1.  open the source code and start editing

    Your site is now running at http://localhost:8000!

    Note: You'll also see a second link: http://localhost:8000/\_\_\_graphql. This is a tool you can use to experiment with querying your data.

    If you want to edit: open the nabad-landing-page directory in your code editor of choice and edit src/pages/index.js. Save your changes and the browser will update in real time!

1.  Install gatsby helmet for SEO:

    `npm install gatsby-plugin-react-helmet react-helmet`

    then add the below configurations to the plugins array in gatsby-config.js:

    `plugins: ["gatsby-plugin-react-helmet"]`

1.  Install gatsby-plugin-anchor-links for smooth navigation:

    `npm install gatsby-plugin-anchor-links`

    then add the below configurations to the plugins array in gatsby-config.js:

    `plugins: { resolve: "gatsby-plugin-anchor-links", options: { offset: -140 } }`

1.  Install gatsby-plugin-routes for customized routing:

    `npm install gatsby-plugin-routes`

    then add the below configurations to the plugins array in gatsby-config.js:

    `plugins: { resolve: `gatsby-plugin-routes`, options: { path: `\${\_\_dirname}/src/routes.js`, } },`

1. install gatsby-plugin-smoothscroll for smooth scrolling:

    `npm install gatsby-plugin-smoothscroll`

    then add the below to the plugins array in gatsby-config.js:

    `plugins: gatsby-plugin-smoothscroll`

1.  Install react-bootstrap for bootstrap components and grid layout:

    `npm install react-bootstrap`

1.  Install react-burger-menu for mobile burger menu:

    `npm install react-burger-menu`

1. Install formik for form validation:

    `npm install formik`

1. Install yup for form validation schema:

    `npm install yup`

1. Install axios to perform HTTP requests:

    `npm install axios`