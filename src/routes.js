// In your routes configuration file
const path = require("path");
module.exports = [
  {
    path: "/",
    component: path.resolve(`src/pages/index.js`),
  },
  {
    path: "/apply-now",
    component: path.resolve(`src/pages/index.js`),
  },
  {
    path: "/about",
    component: path.resolve(`src/pages/index.js`),
  },
  {
    path: "/goals",
    component: path.resolve(`src/pages/index.js`),
  },
  {
    path: "/projects",
    component: path.resolve(`src/pages/index.js`),
  },
];
