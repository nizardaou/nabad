import React from "react";
import { Col, Row, Container } from "react-bootstrap";

import "./HeroCall.css";

export default function HeroCall(props) {
  return (
    <Container fluid id={props.id}>
      <Row className="justify-content-center no-gutters">
        <Col lg={10} align="center">
          <h2>
            Call for <span className="orange-color">Artists</span>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center no-gutters">
        <Col lg={8} md={10} align="center">
          <p className="footer-first-paragraph">
            Nabad is happy to launch the Arleb digital art gallery in February
            2021 for artists in Lebanon. Art categories include: Visual Arts
            (painting, sculpture, drawing, photography, print, illustration…),
            Decorative Arts (glassware, ceramics, pottery, mosaic, textile &
            clothing, jewelry, metalware, furniture…), and Performing Arts
            (music composition, short movies…).
          </p>
          <p className="footer-second-paragraph">
            Deadline to fill this form is January 30, 2021. Selected artists
            will be contacted for further information about the submission
            guidelines and usage rights. Note that there are no submission fees.
          </p>
          <button className="footer-button" onClick={props.apply}>
            Apply Now
          </button>
        </Col>
      </Row>
    </Container>
  );
}
